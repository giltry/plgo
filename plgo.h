#include "postgres.h"
#include "fmgr.h"
#include "pgtime.h"
#include "access/htup_details.h"
#include "catalog/pg_type.h"
#include "utils/builtins.h"
#include "utils/date.h"
#include "utils/timestamp.h"
#include "utils/array.h"
#include "utils/elog.h"
#include "executor/spi.h"
#include "parser/parse_type.h"
#include "commands/trigger.h"
#include "utils/rel.h"
#include "utils/lsyscache.h"
#include "utils/jsonb.h"

#ifndef PLGO
#define PLGO

int varsize(void *var);
void elog_notice(char* string);
void elog_error(char* string);
Datum get_arg(PG_FUNCTION_ARGS, uint i);
HeapTuple get_heap_tuple(HeapTuple* ht, uint i);
Datum get_col_as_datum(HeapTuple ht, TupleDesc td, int colnumber);
bool called_as_trigger(PG_FUNCTION_ARGS);
Datum get_heap_getattr(HeapTuple ht, uint i, TupleDesc td);
Datum void_datum();
Datum cstring_to_datum(char *val);
Datum int16_to_datum(int16 val);
Datum uint16_to_datum(uint16 val);
Datum int32_to_datum(int32 val);
Datum uint32_to_datum(uint32 val);
Datum int64_to_datum(int64 val);
Datum date_to_datum(DateADT val);
Datum time_to_datum(TimeADT val);
Datum timetz_to_datum(TimestampTz val);
Datum bool_to_datum(bool val);
Datum float4_to_datum(float val);
Datum float8_to_datum(double val);
Datum heap_tuple_to_datum(HeapTuple val);
Datum array_to_datum(Oid element_type, Datum* vals, int size);
Datum jsonb_to_datum(char* val);
char* datum_to_cstring(Datum val);
int16 datum_to_int16(Datum val);
uint16 datum_to_uint16(Datum val);
int32 datum_to_int32(Datum val);
uint32 datum_to_uint32(Datum val);
int64 datum_to_int64(Datum val);
DateADT datum_to_date(Datum val);
Timestamp datum_to_time(Datum val);
TimestampTz datum_to_timetz(Datum val);
bool datum_to_bool(Datum val);
float datum_to_float4(Datum val);
double datum_to_float8(Datum val);
HeapTuple datum_to_heap_tuple(Datum val);
Datum* datum_to_array(Datum val, int* nelemsp);
char* unknown_to_char(Datum val);
char* datum_to_jsonb_cstring(Datum val);
bool trigger_fired_before(TriggerEvent tg_event);
bool trigger_fired_after(TriggerEvent tg_event);
bool trigger_fired_instead(TriggerEvent tg_event);
bool trigger_fired_for_row(TriggerEvent tg_event);
bool trigger_fired_for_statement(TriggerEvent tg_event);
bool trigger_fired_by_insert(TriggerEvent tg_event);
bool trigger_fired_by_update(TriggerEvent tg_event);
bool trigger_fired_by_delete(TriggerEvent tg_event);
bool trigger_fired_by_truncate(TriggerEvent tg_event);

#endif