package main

import (
	"flag"
	"fmt"
	"go/build"
	"os"
	"os/exec"
	"path/filepath"
)

func printUsage() {
	fmt.Println(`Usage: plgo [path/to/package]`)
}

func buildPackage(buildPath, packageName string) error {
	if err := os.Setenv("CGO_LDFLAGS_ALLOW", "-shared"); err != nil {
		return err
	}
	if err := os.Setenv("CGO_CFLAGS", "-I . -I /usr/include/postgresql/server"); err != nil {
		return err
	}

	goBuild := exec.Command("go", "build", "-buildmode=c-shared",
		"-o", filepath.Join("build", packageName+".so"),
		filepath.Join(buildPath, "package.go"),
		filepath.Join(buildPath, "methods.go"),
		filepath.Join(buildPath, "pl.go"),
	)
	goBuild.Stdout = os.Stdout
	goBuild.Stderr = os.Stderr
	if err := goBuild.Run(); err != nil {
		return fmt.Errorf("Cannot build package: %s", err)
	}
	return nil
}

func main() {
	flag.Parse()
	packagePath := "."
	if len(flag.Args()) == 1 {
		packagePath = flag.Arg(0)
	}
	moduleWriter, err := NewModuleWriter(packagePath)
	if err != nil {
		fmt.Println(err)
		printUsage()
		return
	}
	tempPackagePath, err := moduleWriter.WriteModule()
	if err != nil {
		fmt.Println(err)
		return
	}

	if _, err = os.Stat("build"); os.IsNotExist(err) {
		err = os.Mkdir("build", 0744)
		if err != nil {
			fmt.Println(err)
			return
		}
	}

	goPath := os.Getenv("GOPATH")
	if goPath == "" {
		goPath = build.Default.GOPATH // Go 1.8 and later have a default GOPATH
	}

	packagePath = goPath + "/src/gitlab.com/giltry/plgo"

	// copy C header and source
	cpCmd := exec.Command("cp", packagePath+"/plgo.h", tempPackagePath+"/plgo.h")
	cpCmd.Stdout = os.Stdout
	cpCmd.Stderr = os.Stderr
	if err := cpCmd.Run(); err != nil {
		fmt.Errorf("%s", err.Error())
		return
	}

	cpCmd = exec.Command("cp", packagePath+"/plgo.c", tempPackagePath+"/plgo.c")
	cpCmd.Stdout = os.Stdout
	cpCmd.Stderr = os.Stderr
	if err := cpCmd.Run(); err != nil {
		fmt.Errorf("%s", err.Error())
		return
	}

	err = buildPackage(tempPackagePath, moduleWriter.PackageName)
	if err != nil {
		fmt.Println(err)
		return
	}
	err = moduleWriter.WriteSQL("build")
	if err != nil {
		fmt.Println(err)
		return
	}
	err = moduleWriter.WriteControl("build")
	if err != nil {
		fmt.Println(err)
		return
	}
	err = moduleWriter.WriteMakefile("build")
	if err != nil {
		fmt.Println(err)
		return
	}
}
