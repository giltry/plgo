#include "plgo.h"

#ifdef PG_MODULE_MAGIC
PG_MODULE_MAGIC;
#endif

int varsize(void *var) {
    return VARSIZE(var);
}

void elog_notice(char* string) {
    elog(NOTICE, string, "");
}

void elog_error(char* string) {
    elog(ERROR, string, "");
}

Datum get_arg(PG_FUNCTION_ARGS, uint i) {
    return PG_GETARG_DATUM(i);
}

HeapTuple get_heap_tuple(HeapTuple* ht, uint i) {
    return ht[i];
}

Datum get_col_as_datum(HeapTuple ht, TupleDesc td, int colnumber) {
    bool isNull;
    Datum ret = SPI_getbinval(ht, td, colnumber + 1, &isNull);
    if (isNull) PG_RETURN_VOID();
    return ret;
}

bool called_as_trigger(PG_FUNCTION_ARGS) {
    return CALLED_AS_TRIGGER(fcinfo);
}

Datum get_heap_getattr(HeapTuple ht, uint i, TupleDesc td) {
    bool isNull;
    Datum ret = heap_getattr(ht, i, td, &isNull);
    if (isNull) PG_RETURN_VOID();
    return ret;
}


//val to datum//////////////////////////////////////////////////
Datum void_datum(){
    PG_RETURN_VOID();
}

Datum cstring_to_datum(char *val) {
    return CStringGetDatum(cstring_to_text(val));
}

Datum int16_to_datum(int16 val) {
    return Int16GetDatum(val);
}

Datum uint16_to_datum(uint16 val) {
    return UInt16GetDatum(val);
}

Datum int32_to_datum(int32 val) {
    return Int32GetDatum(val);
}

Datum uint32_to_datum(uint32 val) {
    return UInt32GetDatum(val);
}

Datum int64_to_datum(int64 val) {
    return Int64GetDatum(val);
}

Datum date_to_datum(DateADT val){
    return DateADTGetDatum(val);
}

Datum time_to_datum(TimeADT val){
    return TimestampGetDatum(val);
}

Datum timetz_to_datum(TimestampTz val) {
    return TimestampTzGetDatum(val);
}

Datum bool_to_datum(bool val) {
    return BoolGetDatum(val);
}

Datum float4_to_datum(float val) {
    return Float4GetDatum(val);
}

Datum float8_to_datum(double val) {
    return Float8GetDatum(val);
}

Datum heap_tuple_to_datum(HeapTuple val) {
    return PointerGetDatum(val);
}

Datum array_to_datum(Oid element_type, Datum* vals, int size) {
    ArrayType *result;
    bool* isnull = (bool *)palloc0(sizeof(bool)*size);
    int dims[1];
    int lbs[1];
    int16 typlen;
    bool typbyval;
    char typalign;

    dims[0] = size;
    lbs[0] = 1;

    // get required info about the element type
    get_typlenbyvalalign(element_type, &typlen, &typbyval, &typalign);
    result = construct_md_array(vals, isnull, 1, dims, lbs,
                                element_type, typlen, typbyval, typalign);

    PG_RETURN_ARRAYTYPE_P(result);
}

Datum jsonb_to_datum(char* val) {
    return (Datum) DatumGetJsonb(DirectFunctionCall1(jsonb_in, (Datum) (char *) val));
}

//Datum to val //////////////////////////////////////////////////////////
char* datum_to_cstring(Datum val) {
    return DatumGetCString(text_to_cstring((struct varlena *)val));
}

int16 datum_to_int16(Datum val) {
    return DatumGetInt16(val);
}

uint16 datum_to_uint16(Datum val) {
    return DatumGetUInt16(val);
}

int32 datum_to_int32(Datum val) {
    return DatumGetInt32(val);
}

uint32 datum_to_uint32(Datum val) {
    return DatumGetUInt32(val);
}

int64 datum_to_int64(Datum val) {
    return DatumGetInt64(val);
}

DateADT datum_to_date(Datum val) {
    return DatumGetDateADT(val);
}

Timestamp datum_to_time(Datum val) {
    return DatumGetTimestamp(val);
}

TimestampTz datum_to_timetz(Datum val) {
    return DatumGetTimestampTz(val);
}

bool datum_to_bool(Datum val) {
    return DatumGetBool(val);
}

float datum_to_float4(Datum val) {
    return DatumGetFloat4(val);
}

double datum_to_float8(Datum val) {
    return DatumGetFloat8(val);
}

HeapTuple datum_to_heap_tuple(Datum val) {
    return (HeapTuple) DatumGetPointer(val);
}

Datum* datum_to_array(Datum val, int* nelemsp) {
    ArrayType* array = DatumGetArrayTypeP(val);

    int16 typlen;
    bool typbyval;
    char typalign;
    Datum *result;
    bool *nullsp;

    get_typlenbyvalalign(ARR_ELEMTYPE(array), &typlen, &typbyval, &typalign);

    deconstruct_array(array, ARR_ELEMTYPE(array),
                      typlen, typbyval, typalign,
                      &result, &nullsp, nelemsp);
    return result;
}

char* unknown_to_char(Datum val) {
    return (char*)val;
}

char* datum_to_jsonb_cstring(Datum val) {
    Jsonb *jsonb = DatumGetJsonb(val);
    return JsonbToCString(NULL, &jsonb->root, VARSIZE(jsonb));
}

//TriggerData functions/////////////////////////////////////////////
bool trigger_fired_before(TriggerEvent tg_event) {
    return TRIGGER_FIRED_BEFORE(tg_event);
}

bool trigger_fired_after(TriggerEvent tg_event) {
    return TRIGGER_FIRED_AFTER(tg_event);
}

bool trigger_fired_instead(TriggerEvent tg_event) {
    return TRIGGER_FIRED_INSTEAD(tg_event);
}

bool trigger_fired_for_row(TriggerEvent tg_event) {
    return TRIGGER_FIRED_FOR_ROW(tg_event);
}

bool trigger_fired_for_statement(TriggerEvent tg_event) {
    return TRIGGER_FIRED_FOR_STATEMENT(tg_event);
}

bool trigger_fired_by_insert(TriggerEvent tg_event) {
    return TRIGGER_FIRED_BY_INSERT(tg_event);
}

bool trigger_fired_by_update(TriggerEvent tg_event) {
    return TRIGGER_FIRED_BY_UPDATE(tg_event);
}

bool trigger_fired_by_delete(TriggerEvent tg_event) {
    return TRIGGER_FIRED_BY_DELETE(tg_event);
}

bool trigger_fired_by_truncate(TriggerEvent tg_event) {
    return TRIGGER_FIRED_BY_TRUNCATE(tg_event);
}